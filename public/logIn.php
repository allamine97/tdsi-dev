<!DOCTYPE html>
<html>
<head>
    <?php require_once 'layout/header.php'; ?>
    <title>Connexion</title>
</head>
<body>

    <div class="container marge-top-max">
        <div class="alert alert-secondary">
            <strong>Connexion</strong>
        </div>
        <hr>
        <div class="row marge-top-min">
            <div class="col-md-2"></div>
            <div class="col-md-6">
                <form action="controller.php" method="post">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="login" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Password:</label>
                        <input type="password" name="mot_de_passe" class="form-control" id="pwd">
                    </div>
                    <button type="submit" name="connect" class="btn btn-success">Se Connecter</button>
                </form>
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row justify-content-center marge-top-min">
        	<p class="text-center">
        		Si vous ne possédez pas un compte veuillez vous 
        		<a href="signIn.php" class="btn btn-primary">Inscrire</a>
        	</p>
        </div>
    </div>
    
</body>
</html>