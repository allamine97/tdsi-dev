<?php require_once 'layout/security.php';  ?>
<?php require_once 'src/etudiantCRUD.php'; ?>
<?php $crud = new EtudiantCRUD(); ?>
<?php $listEtudiants = $crud->getList(); ?>
<!DOCTYPE html>
<html>
<head>
    <?php require_once 'layout/header.php'; ?>
    <title>Liste Etudiants</title>
</head>
<body>
    <?php require_once 'layout/navbar.php'; ?>
    <div class="container marge-top-max">
        <div class="alert alert-secondary">
            <strong>Liste des Etudiants</strong>
        </div>
        <hr>
        <div class="row marge-top-max">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <table class="table table-striped table-hover ">
                    <thead class="thead-dark">
                        <tr>
                            <th>N° Carte</th>
                            <th>Prénom </th>
                            <th>Nom </th>
                            <th>Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($listEtudiants as $etudiant) { ?>
                            <tr>
                            	<td><?php echo $etudiant->getNumCarte(); ?></td>
                            	<td><?php echo $etudiant->getPrenom(); ?></td>
                            	<td><?php echo $etudiant->getNom(); ?></td>
                            	<td><?php echo $etudiant->getEmail(); ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>


</body>
</html>