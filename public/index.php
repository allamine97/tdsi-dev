<?php require_once 'layout/security.php';  ?>
<!DOCTYPE html>
<html>
<head>
    <?php require_once 'layout/header.php'; ?>
    <title>Welcome</title>
</head>
<body>

	<?php require_once 'layout/navbar.php'; ?>

	<div class="jumbotron text-center">
        <h1>Welcome to TDSI</h1>
        <p>Entreprise L3 TDSI MCS 2019</p>
    </div>
          
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <h3>Developer Department</h3>
                <p>Lorem ipsum dolor..</p>
            </div>
            <div class="col-sm-4">
                <h3>Networking Department</h3>
                <p>Lorem ipsum dolor..</p>
            </div>
            <div class="col-sm-4">
                <h3>Security Department</h3>
                <p>Lorem ipsum dolor..</p>
            </div>
        </div>
    </div>

</body>
</html>