
<!DOCTYPE html>
<html>
<head>
    <?php require_once 'layout/header.php'; ?>
    <title>Inscription</title>
</head>
<body>

    <div class="container marge-top-max">
        <div class="alert alert-secondary">
            <strong>Formulaire D'inscription</strong>
        </div>
        <hr>
        <div class="row">
        
        	<div class="col-md-1"></div>
        	
        	<!-- Design -->
            <div class="col-md-5">
            	<div id="demo" class="carousel slide" data-ride="carousel">
            	
					<!-- Indicators -->
					<ul class="carousel-indicators">
						<li data-target="#demo" data-slide-to="0" class="active"></li>
						<li data-target="#demo" data-slide-to="1"></li>
						<li data-target="#demo" data-slide-to="2"></li>
					</ul>

					<!-- The slideshow -->
					<div class="carousel-inner">
						<div class="carousel-item">
							<img src="img/Jobs.jpg" class="img-dimension">
						</div>
						<div class="carousel-item active">
							<img src="img/network-department.jpg" class="img-dimension">
						</div>
						<div class="carousel-item">
							<img src="img/prog.jpg" class="img-dimension">
						</div>
					</div>

					<!-- Left and right controls -->
					<a class="carousel-control-prev" href="#demo" data-slide="prev"> <span
						class="carousel-control-prev-icon"></span>
					</a> <a class="carousel-control-next" href="#demo"
						data-slide="next"> <span class="carousel-control-next-icon"></span>
					</a>

				</div>
            </div>
            
            <div class="col-md-1"></div>
            
            <!-- Formulaire -->
            <div class="col-md-4">
            	<h5 class="text-center">Inscrivez vous</h5>
                <form action="controller.php" method="post">
                    <div class="form-group">
                        <input placeholder="Prénom" type="text" name="prenom" class="form-control">
                    </div>
                    <div class="form-group">
                        <input placeholder="Nom" type="text" name="nom" class="form-control">
                    </div>
                    <div class="form-group">
                        <input placeholder="Numéro de Carte" type="text" name="num_carte" class="form-control">
                    </div>
                    <div class="form-group">
                        <input placeholder="Email" type="text" name="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <input placeholder="Mot de passe" type="password" name="mot_de_passe" class="form-control">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success" name="add_etudiant">Soumettre</button>
                        <button type="reset" class="btn btn-danger">Reset</button>
                	</div>
                </form>
            </div>
            
            <div class="col-md-1"></div>
            
        </div>
        <div class="row justify-content-center marge-top-min">
        	<p class="text-center">Si vous possédez déjà un compte veuillez vous <a href="logIn.php" class="btn btn-primary">connecter</a></p>
        </div>
    </div>
    
</body>
</html>