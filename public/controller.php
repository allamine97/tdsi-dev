<?php 
require_once 'src/etudiantCRUD.php';

$crud = new EtudiantCRUD();

session_start();

    //----------- signIn.php Controller
    if (isset($_POST['add_etudiant'])) {
        $etd = new Etudiant();
        $etd->setNumCarte($_POST['num_carte']);
        $etd->setNom($_POST['nom']);
        $etd->setPrenom($_POST['prenom']);
        $etd->setEmail($_POST['email']);
        $etd->setMotDePasse($_POST['mot_de_passe']);
    
        $execute = $crud->addEtudiant($etd);
        if ($execute == true) {
            header("location:list.php");
        } else {
            header("location:signIn.php");
        }
    }
    //----------- end signIn.php Controller

    //----------- logIn.php Controller
    if (isset($_POST['connect'])) {
        $login = $_POST['login'];
        $mdp = $_POST['mot_de_passe'];
        $cnx = $crud->connect($login,$mdp);
        if ($cnx != null) {
            $_SESSION['PROFIL'] = $cnx;
            header("location:index.php");
        } else {
            header("location:logIn.php");
        }
    }
    //----------- end logIn.php Controller

?>