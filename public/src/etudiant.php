<?php

class Etudiant {
    
    private $nom;
    private $prenom;
    private $email;
    private $numCarte;
    private $motDePasse;

    public function getNom()
    {
        return $this->nom;
    }
    
    public function getPrenom()
    {
        return $this->prenom;
    }
    
    public function getEmail()
    {
        return $this->email;
    }
    
    public function getNumCarte()
    {
        return $this->numCarte;
    }
    
    public function setNom($nom)
    {
        $this->nom = $nom;
    }
    
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }
    
    public function setEmail($email)
    {
        $this->email = $email;
    }
    
    public function setNumCarte($numCarte)
    {
        $this->numCarte = $numCarte;
    }
    
    public function getMotDePasse()
    {
        return $this->motDePasse;
    }
    
    public function setMotDePasse($motDePasse)
    {
        $this->motDePasse = $motDePasse;
    }

    public  function __construct() {
        
    }
    
    
}