<?php 

require_once 'etudiant.php';
require_once 'connectionDB.php';


class  EtudiantCRUD {
    
    
    public function __construct() {}
    
    public function addEtudiant(Etudiant $etd) {
        try {
            $cnxClass = new ConnectionDB();
            $pdo = $cnxClass->getPdo();
            $stmt = $pdo->prepare
            ("INSERT INTO etudiant (num_carte,nom,prenom,email,mot_de_passe) VALUES (?,?,?,?,?)");
            $params = array(
                $etd->getNumCarte(),
                $etd->getNom(),
                $etd->getPrenom(),
                $etd->getEmail(),
                $etd->getMotDePasse()
            );
            $stmt->execute($params);
            return true;
        } catch (PDOException $e) {
            echo ($e->getMessage());
            return false;
        }
    }
    
    public function connect($login, $motDePasse) {
        $response = null;
        try {
            $cnxClass = new ConnectionDB();
            $pdo = $cnxClass->getPdo();
            $stmt = $pdo->prepare("SELECT * FROM etudiant");
            $stmt->execute();
            while ($row = $stmt->fetch()) {
                $log = $row['email'];
                $mdp = $row['mot_de_passe'];
                if ($login == $log && $motDePasse == $mdp) {
                    $response = new Etudiant();
                    $response->setNumCarte($row['num_carte']);
                    $response->setNom($row['nom']);
                    $response->setPrenom($row['prenom']);
                    $response->setEmail($row['email']);
                    break;
                }
            }
        } catch (PDOException $e) {
            echo ($e->getMessage());
        }
        return  $response;
    }
    
    public function getList() {
        try {
            $etds = array();
            $cnxClass = new ConnectionDB();
            $pdo = $cnxClass->getPdo();
            $stmt = $pdo->prepare("SELECT * FROM etudiant");
            $stmt->execute();
            while ($row = $stmt->fetch()) {
                $etd = new Etudiant();
                $etd->setNumCarte($row['num_carte']);
                $etd->setNom($row['nom']);
                $etd->setPrenom($row['prenom']);
                $etd->setEmail($row['email']);
                $etds[] = $etd;
            }
            return $etds;
        } catch (PDOException $e) {
            echo ($e->getMessage());
            return null;
        }
    }
}

?>